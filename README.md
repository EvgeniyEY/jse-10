# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EVGENIY ERMOLAEV

**E-MAIL**: ermolaev.evgeniy.96@yandex.ru

# SOFTWARE

- JDK 1.8

- Windows 10

# PROGRAM BUILD

```bash
mvn clean install
```

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/d/LLSAu0b7C4pz1g?w=1