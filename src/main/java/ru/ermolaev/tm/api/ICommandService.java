package ru.ermolaev.tm.api;

import ru.ermolaev.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
