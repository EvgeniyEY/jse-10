package ru.ermolaev.tm.api;

public interface ITerminalController {

    void parseArg(final String arg);

    void parseCommand(final String arg);

    boolean parseArgs(final String[] args);

    void showHelp();

    void showCommands();

    void showArguments();

    void showAbout();

    void showVersion();

    void showInfo();

    void exit();

}
