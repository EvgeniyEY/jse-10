package ru.ermolaev.tm;

import ru.ermolaev.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
