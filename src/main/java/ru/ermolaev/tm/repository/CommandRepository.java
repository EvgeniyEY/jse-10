package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.ICommandRepository;
import ru.ermolaev.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            TerminalCommand.HELP,
            TerminalCommand.ABOUT,
            TerminalCommand.VERSION,
            TerminalCommand.INFO,
            TerminalCommand.EXIT,
            TerminalCommand.COMMANDS,
            TerminalCommand.ARGUMENTS
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand value : values) {
            final String name = value.getCommand();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand value : values) {
            final String arg = value.getArgument();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}
