package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.ICommandService;
import ru.ermolaev.tm.api.ITerminalController;
import ru.ermolaev.tm.constant.ITerminalConst;
import ru.ermolaev.tm.constant.IArgumentConst;
import ru.ermolaev.tm.model.TerminalCommand;
import ru.ermolaev.tm.util.NumberUtil;

public class TerminalController implements ITerminalController {

    private final ICommandService commandService;

    public TerminalController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void parseArg(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case IArgumentConst.HELP:
                showHelp();
                break;
            case IArgumentConst.ABOUT:
                showAbout();
                break;
            case IArgumentConst.VERSION:
                showVersion();
                break;
            case IArgumentConst.INFO:
                showInfo();
                break;
            case IArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case IArgumentConst.COMMANDS:
                showCommands();
                break;
            default:
                System.out.println("Invalid argument");
        }
    }

    public void parseCommand(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case ITerminalConst.HELP:
                showHelp();
                break;
            case ITerminalConst.ABOUT:
                showAbout();
                break;
            case ITerminalConst.VERSION:
                showVersion();
                break;
            case ITerminalConst.INFO:
                showInfo();
                break;
            case ITerminalConst.EXIT:
                exit();
            case ITerminalConst.COMMANDS:
                showCommands();
                break;
            case ITerminalConst.ARGUMENTS:
                showArguments();
                break;
            default:
                System.out.println("Invalid command");
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = commandService.getTerminalCommands();
        for (final TerminalCommand command: commands) System.out.println(command);
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        for (final String command: commands) System.out.println(command);
    }

    public void showArguments() {
        final String[] arguments = commandService.getArgs();
        for (final String argument: arguments) System.out.println(argument);
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Evgeniy Ermolaev");
        System.out.println("E-MAIL: ermolaev.evgeniy.96@yandex.ru");
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.10");
    }

    public void showInfo() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(Runtime.getRuntime().freeMemory()));
        System.out.println("Maximum memory: " + (Runtime.getRuntime().maxMemory() == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(Runtime.getRuntime().maxMemory())));
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()));
    }

    public void exit() {
        System.exit(0);
    }

}
