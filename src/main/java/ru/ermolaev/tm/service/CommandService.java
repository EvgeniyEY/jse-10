package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.ICommandRepository;
import ru.ermolaev.tm.api.ICommandService;
import ru.ermolaev.tm.model.TerminalCommand;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}
