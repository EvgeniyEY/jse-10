package ru.ermolaev.tm.bootstrap;

import ru.ermolaev.tm.api.ICommandRepository;
import ru.ermolaev.tm.api.ICommandService;
import ru.ermolaev.tm.api.ITerminalController;
import ru.ermolaev.tm.controller.TerminalController;
import ru.ermolaev.tm.repository.CommandRepository;
import ru.ermolaev.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITerminalController terminalController = new TerminalController(commandService);

    public void run(final String[] args) {
        System.out.println("Welcome to task manager");
        if (terminalController.parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            terminalController.parseCommand(command);
        }
    }

}
